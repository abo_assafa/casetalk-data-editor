/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casemice.data.editor;

import com.casemice.data.packages.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author alisafaya
 */
public class DataEditor extends javax.swing.JFrame {

    private ProjectPackage mainPackage;
    private String dataSetFilePath;


    /**
     * Creates new form DataEditor
     */
    public DataEditor() {
        initComponents();
        this.setTitle("Data editor @casemice");
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                if (!mainPackage.dataSet.isEmpty()) {
                    if (JOptionPane.showConfirmDialog(DataEditor.this,
                            "Are you sure? you may need to save current project before quitting\n To quit click Yes, To save current project click No", "Exit",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }
                } else {
                    System.exit(0);
                }
            }
        });                
                         
        this.dataClassesList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    // Double-click detected
                    int index = dataClassesList.locationToIndex(evt.getPoint());
                    if (index == -1) {
                        return;
                    }
                    String input = promptForString(dataClassesList.getSelectedValue().label, "", "Edit categories name");

                    if (input.isEmpty()) {
                        return;
                    }
                    dataClassesList.getSelectedValue().label = input;
                    reloadDataClassesList();
                }
            }
        });

        this.dataSamplesList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    // Double-click detected
                    int index = dataSamplesList.locationToIndex(evt.getPoint());
                    if (index == -1) {
                        return;
                    }
                    String input = promptForString(dataSamplesList.getSelectedValue().content, "", "Edit data sample");
                    if (input.isEmpty()) {
                        return;
                    }
                    dataSamplesList.getSelectedValue().content = input;
                    reloadDataSamplesList();
                }
            }
        });

        this.casesList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    // Double-click detected
                    int index = casesList.locationToIndex(evt.getPoint());
                    if (index == -1) {
                        return;
                    }
                    String input = promptForString(casesList.getSelectedValue().name, "", "Edit case name");
                    if (input.isEmpty()) {
                        return;
                    }
                    casesList.getSelectedValue().name = input;
                    reloadCasesList();
                }
            }
        });

        this.casesAnswersTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    // Double-click detected
                    int index = casesAnswersTable.rowAtPoint(evt.getPoint());
                    if (index == -1) {
                        return;
                    }
                    int Id = Integer.parseInt(casesAnswersTable.getModel().getValueAt(index, 0).toString());
                    String input = promptForString(casesList.getSelectedValue().answers.get(Id) == null ? "" : casesList.getSelectedValue().answers.get(Id).content,
                            "", "Edit answer");
                    if (input.isEmpty()) {
                        return;
                    }
                    if (casesList.getSelectedValue().answers.get(Id) == null) {
                        casesList.getSelectedValue().answers.put(Id, new Sample(Id, input));
                    } else {
                        casesList.getSelectedValue().answers.get(Id).content = input;
                    }
                    reloadCasesAnswersTable();
                }
            }
        });

        this.mainPackage = new ProjectPackage();
        this.mainPackage.dataSet = new TreeMap<Integer, DataClass>();
        this.mainPackage.cases = new LinkedList<Case>();
        this.dataSetFilePath = "";
    }

    private static void infoBox(String infoMessage, String titleBar) {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

    private String promptForString(String content, String title, String message) {
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.Y_AXIS));
        inputPanel.add(new JLabel(message +'\n'));
        JTextField inputField = new JTextField(20);
        inputField.setText(content);
        inputField.addMouseListener(new RightClickMouseListener());
        inputPanel.add(inputField);

        int input = JOptionPane.showConfirmDialog(null, inputPanel,
            title, JOptionPane.OK_CANCEL_OPTION);
       
        if (input != JOptionPane.OK_OPTION) {
            return "";
        } else {
           return inputField.getText();
        }
    }

    private void reloadDataClassesList() {
        DefaultListModel<DataClass> model = new DefaultListModel<>();
        this.mainPackage.dataSet.values().forEach((item) -> {
            model.addElement(item);
        });
        this.dataClassesList.setModel(model);
    }

    private void reloadDataSamplesList() {
        DefaultListModel<Sample> model = new DefaultListModel<>();
        if (this.dataClassesList.getSelectedValue() != null) {
            this.dataClassesList.getSelectedValue().samples.values().forEach((item) -> {
                model.addElement(item);
            });
        }
        this.dataSamplesList.setModel(model);
    }

    private void reloadCasesList() {
        DefaultListModel<Case> model = new DefaultListModel<>();
        this.mainPackage.cases.forEach((item) -> {
            model.addElement(item);
        });
        this.casesList.setModel(model);
        reloadCasesAnswersTable();
    }

    private void reloadCasesAnswersTable() {

        List<Object[]> rows = new ArrayList<>();
        DefaultTableModel model;
        if (this.casesList.getSelectedValue() != null) {
            for (Integer key : this.mainPackage.dataSet.keySet()) {
                Object[] row = new Object[]{
                    this.mainPackage.dataSet.get(key).classId,
                    this.mainPackage.dataSet.get(key).label,
                    this.casesList.getSelectedValue().answers.get(key) == null ? "----Daha Eklenmemiş----" : this.casesList.getSelectedValue().answers.get(key).content
                };
                rows.add(row);
            }
            Object[][] rowArray = rows.toArray(new Object[rows.size()][3]);
            model = new DefaultTableModel(rowArray, new Object[]{
                "Id",
                "Category",
                "Answer"
            }) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    //all cells false
                    return false;
                }
            };
            this.casesAnswersTable.setModel(model);
        } else {
            model = new DefaultTableModel();
            this.casesAnswersTable.setModel(model);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        dataSetFilePathLabel = new javax.swing.JLabel();
        EditorPanel = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        DataClasses = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dataClassesList = new javax.swing.JList<>();
        addDataClassButton = new javax.swing.JButton();
        deleteSelectedDataClassButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        dataSamplesList = new javax.swing.JList<>();
        addDataSampleButton = new javax.swing.JButton();
        deleteSelectedSampleButton = new javax.swing.JButton();
        casesPanel = new javax.swing.JPanel();
        jSeparator2 = new javax.swing.JSeparator();
        jSplitPane2 = new javax.swing.JSplitPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        casesList = new javax.swing.JList<>();
        jPanel4 = new javax.swing.JPanel();
        addNewCaseButton = new javax.swing.JButton();
        deleteSelectedCaseButton = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        casesAnswersTable = new javax.swing.JTable();
        DataEditorMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newProjectMenuItem = new javax.swing.JMenuItem();
        openProjectMenuItem = new javax.swing.JMenuItem();
        saveAsProjectMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        openDatasetMenuItem = new javax.swing.JMenuItem();
        addCaseMenuItem = new javax.swing.JMenuItem();
        exportMenu = new javax.swing.JMenu();
        exportDatasetMenuItem = new javax.swing.JMenuItem();
        exportCasesForEngineMenuItem = new javax.swing.JMenuItem();
        exportCasesForCasemobileMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 600));

        dataSetFilePathLabel.setText("Project : not saved yet");

        jSplitPane1.setDividerLocation(410);
        jSplitPane1.setDividerSize(4);

        dataClassesList.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        dataClassesList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dataClassesList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dataClassesListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(dataClassesList);

        addDataClassButton.setText("Add new category");
        addDataClassButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDataClassButtonActionPerformed(evt);
            }
        });

        deleteSelectedDataClassButton.setText("Delete selected category");
        deleteSelectedDataClassButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteSelectedDataClassButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
                    .addComponent(deleteSelectedDataClassButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(addDataClassButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addDataClassButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deleteSelectedDataClassButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel2);

        dataSamplesList.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        dataSamplesList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(dataSamplesList);

        addDataSampleButton.setText("Add new sample");
        addDataSampleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDataSampleButtonActionPerformed(evt);
            }
        });

        deleteSelectedSampleButton.setText("Delete selected sample");
        deleteSelectedSampleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteSelectedSampleButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
                    .addComponent(deleteSelectedSampleButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(addDataSampleButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addDataSampleButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deleteSelectedSampleButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setRightComponent(jPanel3);

        javax.swing.GroupLayout DataClassesLayout = new javax.swing.GroupLayout(DataClasses);
        DataClasses.setLayout(DataClassesLayout);
        DataClassesLayout.setHorizontalGroup(
            DataClassesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        DataClassesLayout.setVerticalGroup(
            DataClassesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DataClassesLayout.createSequentialGroup()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Categories", DataClasses);

        jSplitPane2.setDividerLocation(410);

        casesList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        casesList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                casesListValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(casesList);

        jSplitPane2.setLeftComponent(jScrollPane3);

        addNewCaseButton.setText("Add new case");
        addNewCaseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addNewCaseButtonActionPerformed(evt);
            }
        });

        deleteSelectedCaseButton.setText("Delete selected case");
        deleteSelectedCaseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteSelectedCaseButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addNewCaseButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deleteSelectedCaseButton, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addNewCaseButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deleteSelectedCaseButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane2.setRightComponent(jPanel4);

        casesAnswersTable.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        casesAnswersTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane4.setViewportView(casesAnswersTable);

        javax.swing.GroupLayout casesPanelLayout = new javax.swing.GroupLayout(casesPanel);
        casesPanel.setLayout(casesPanelLayout);
        casesPanelLayout.setHorizontalGroup(
            casesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE)
            .addGroup(casesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(casesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2))
                .addContainerGap())
        );
        casesPanelLayout.setVerticalGroup(
            casesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(casesPanelLayout.createSequentialGroup()
                .addComponent(jSplitPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 525, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Cases", casesPanel);

        javax.swing.GroupLayout EditorPanelLayout = new javax.swing.GroupLayout(EditorPanel);
        EditorPanel.setLayout(EditorPanelLayout);
        EditorPanelLayout.setHorizontalGroup(
            EditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        EditorPanelLayout.setVerticalGroup(
            EditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        fileMenu.setText("File");

        newProjectMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.META_MASK));
        newProjectMenuItem.setText("New");
        newProjectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newProjectMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newProjectMenuItem);

        openProjectMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.META_MASK));
        openProjectMenuItem.setText("Open");
        openProjectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openProjectMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openProjectMenuItem);

        saveAsProjectMenuItem.setText("Save as ...");
        saveAsProjectMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsProjectMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsProjectMenuItem);

        saveMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.META_MASK));
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        exitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.META_MASK));
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        DataEditorMenuBar.add(fileMenu);

        jMenu2.setText("Edit");

        openDatasetMenuItem.setText("Add dataset from Interviewer");
        openDatasetMenuItem.setToolTipText("");
        openDatasetMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openDatasetMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(openDatasetMenuItem);

        addCaseMenuItem.setText("Add case");
        addCaseMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCaseMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(addCaseMenuItem);

        DataEditorMenuBar.add(jMenu2);

        exportMenu.setText("Export");

        exportDatasetMenuItem.setText("Export dataset for engine");
        exportDatasetMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportDatasetMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportDatasetMenuItem);

        exportCasesForEngineMenuItem.setText("Export cases for engine");
        exportCasesForEngineMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportCasesForEngineMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportCasesForEngineMenuItem);

        exportCasesForCasemobileMenuItem.setText("Export cases for casemobile ");
        exportCasesForCasemobileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportCasesForCasemobileMenuItemActionPerformed(evt);
            }
        });
        exportMenu.add(exportCasesForCasemobileMenuItem);

        DataEditorMenuBar.add(exportMenu);

        setJMenuBar(DataEditorMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dataSetFilePathLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(EditorPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dataSetFilePathLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(EditorPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void openDatasetMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openDatasetMenuItemActionPerformed

        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose dataset file");
        fc.setCurrentDirectory(new java.io.File("."));
        fc.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                } else if (f.getName().endsWith(".txt")) {
                    return true;
                } else {
                    return false;
                }
            }

            public String getDescription() {
                return "( *.txt ) data file by Casemice Interviewer.";
            }
        });

        int returnVal = fc.showOpenDialog(DataEditor.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            if (!file.getName().endsWith(".txt")) {
                infoBox("Only .txt files are accepted.\n", "Warning!");
                return;
            }
            int sampleCount = 0, classNumber, lineNo = 0;
            try {

                String line;
                Map<Integer, DataClass> dataClasses = new TreeMap<>();
                FileReader fileReader = new FileReader(file.getAbsolutePath());
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                classNumber = Integer.parseInt(bufferedReader.readLine().split("|")[0]);
                lineNo++;
                String classLabel;
                int classId = -1;

                while ((line = bufferedReader.readLine()) != null) {
                    lineNo++;
                    if (line.charAt(0) == '*') {

                        classId = Integer.parseInt(line.split("\\|")[0].split(" ")[1]);
                        classLabel = line.split("\\|")[1];
                        dataClasses.put(classId, new DataClass(classId, classLabel, new TreeMap<Integer, Sample>()));
                        sampleCount = 0;

                    } else {
                        if (line.isEmpty()) {
                            infoBox("Line " + String.valueOf(lineNo) + " is empty.", "Warning!");
                            continue;
                        }
                        if (dataClasses.containsKey(classId)) {
                            dataClasses.get(classId).samples.put(sampleCount, new Sample(sampleCount++, line));
                        } else {
                            infoBox("Line " + String.valueOf(lineNo) + " is not in the required format.", "Warning!");
                            return;
                        }

                    }
                }
                bufferedReader.close();
                if (dataClasses.keySet().removeAll(this.mainPackage.dataSet.keySet())) {
                    infoBox("Confliction : could not accept given file because current project already has categories with given Id's.", "Warning!");
                    return;
                }
                this.mainPackage.dataSet.putAll(dataClasses);
            } catch (FileNotFoundException e) {
                infoBox("Could not find the given file", "Warning!");
                return;
            } catch (IOException | NumberFormatException e) {
                infoBox("Some error happened while reading the file \nLine :" + lineNo, "Warning!");
                return;
            } catch (IllegalArgumentException e) {
                infoBox("Confliction : could not accept given file because current project already has categories with given Id's.", "Warning!");
                return;
            } catch (Exception e) {
                infoBox("Unknown error: please check your file on line " + String.valueOf(lineNo), "Warning!");
                return;
            }
            reloadDataClassesList();
        }

    }//GEN-LAST:event_openDatasetMenuItemActionPerformed

    private void dataClassesListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dataClassesListValueChanged
        if (!evt.getValueIsAdjusting()) {
            reloadDataSamplesList();
        }
    }//GEN-LAST:event_dataClassesListValueChanged

    private void addDataClassButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDataClassButtonActionPerformed
        String input = promptForString("", "New category", "Add new categories name");
        if (!input.isEmpty()) {
            int maxKey = -1;
            for (Integer key : this.mainPackage.dataSet.keySet()) {
                if (maxKey < key) {
                    maxKey = key;
                }
            }
            maxKey++;
            this.mainPackage.dataSet.put(maxKey, new DataClass(maxKey, input, new TreeMap<Integer, Sample>()));
            reloadDataClassesList();
            reloadCasesAnswersTable();
        }
    }//GEN-LAST:event_addDataClassButtonActionPerformed

    private void deleteSelectedDataClassButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteSelectedDataClassButtonActionPerformed
        if (this.dataClassesList.getSelectedValue() != null) {
            int key = this.dataClassesList.getSelectedValue().classId;
            List<String> affectedCases = new LinkedList<>();
            for (Case casee : mainPackage.cases) {
                if (casee.answers.containsKey(key)) {
                    affectedCases.add(casee.name);
                }
            }
            if (!affectedCases.isEmpty()) {
                if (JOptionPane.showConfirmDialog(DataEditor.this,
                        "The category you are trying to delete has answers associated with it in cases :\n" + String.join("\n", affectedCases) + "\nTo delete click Yes, To cancel click No", "Exit",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            this.mainPackage.dataSet.remove(key);
            reloadDataClassesList();
            reloadCasesAnswersTable();
        }
    }//GEN-LAST:event_deleteSelectedDataClassButtonActionPerformed

    private void addDataSampleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDataSampleButtonActionPerformed
        if (this.dataClassesList.getSelectedValue() != null) {
            DataClass selectedClass = this.dataClassesList.getSelectedValue();
            String input = promptForString("", "New sample", "Add new samples content");
            if (!input.isEmpty()) {
                int maxKey = -1;
                for (Integer key : selectedClass.samples.keySet()) {
                    if (maxKey < key) {
                        maxKey = key;
                    }
                }
                maxKey++;
                selectedClass.samples.put(maxKey, new Sample(maxKey, input));
                reloadDataSamplesList();
            }
        }
    }//GEN-LAST:event_addDataSampleButtonActionPerformed

    private void deleteSelectedSampleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteSelectedSampleButtonActionPerformed
        if (this.dataClassesList.getSelectedValue() != null && this.dataSamplesList.getSelectedValue() != null) {
            DataClass dataClass = this.dataClassesList.getSelectedValue();
            int key = this.dataSamplesList.getSelectedValue().sampleId;
            dataClass.samples.remove(key);
            reloadDataSamplesList();
        }
    }//GEN-LAST:event_deleteSelectedSampleButtonActionPerformed

    private void saveAsProjectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsProjectMenuItemActionPerformed
        if (this.mainPackage.dataSet.values().isEmpty()) {
            infoBox("Cannot save empty project.", "Warning");
            return;
        }

        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose destination file");
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setCurrentDirectory(new java.io.File("."));
        fc.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                return true;
            }

            public String getDescription() {
                return "( *.datedt ) Data-editor project file.";
            }
        });

        int returnVal = fc.showSaveDialog(DataEditor.this);
        if (returnVal != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File outFile = fc.getSelectedFile();

        try {
            FileOutputStream fileOut = new FileOutputStream(outFile.getAbsolutePath() + ".datedt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this.mainPackage);
            out.close();
            fileOut.close();
            this.dataSetFilePathLabel.setText(this.dataSetFilePathLabel.getText().split(":")[0] + " : " + outFile.getName() + ".datedt");
            this.dataSetFilePath = outFile.getAbsolutePath() + ".datedt";
        } catch (IOException e) {
            infoBox("Error while trying to serialize data classes\n" + e.getMessage(), "Error!");
        }

    }//GEN-LAST:event_saveAsProjectMenuItemActionPerformed

    private void openProjectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openProjectMenuItemActionPerformed
        if (!this.mainPackage.dataSet.isEmpty()) {
            if (JOptionPane.showConfirmDialog(DataEditor.this,
                    "Are you sure? you may need to save current project before opening another\n To open project click Yes, To save current project click No", "Open project",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION) {
                return;
            }
        }

        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose data-editor project file");
        fc.setCurrentDirectory(new java.io.File("."));
        fc.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                } else if (f.getName().endsWith(".datedt")) {
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public String getDescription() {
                return "( *.datedt ) Data-editor project file.";
            }
        });

        int returnVal = fc.showOpenDialog(DataEditor.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            if (!file.getName().endsWith(".datedt")) {
                infoBox("Only .datedt files are accepted.\n", "Warning!");
                return;
            }
            try {
                ProjectPackage ret;
                FileInputStream fileIn = new FileInputStream(file);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                ret = (ProjectPackage) in.readObject();
                in.close();
                fileIn.close();
                this.mainPackage = ret;
            } catch (ClassNotFoundException e) {
                infoBox("Given file (" + file.getName() + ") could not be read.\n" + "Details :" + e.getMessage(), "Error");
                return;
            } catch (IOException e) {
                infoBox("Given file (" + file.getName() + ") could not be read.\n" + "Details :" + e.getMessage(), "Error");
                return;
            }
            this.dataSetFilePathLabel.setText(this.dataSetFilePathLabel.getText().split(":")[0] + ": " + file.getName());
            this.dataSetFilePath = file.getAbsolutePath();
            reloadDataClassesList();
            reloadCasesList();
        }
    }//GEN-LAST:event_openProjectMenuItemActionPerformed

    private void newProjectMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newProjectMenuItemActionPerformed
        if (!this.mainPackage.dataSet.isEmpty()) {
            if (JOptionPane.showConfirmDialog(DataEditor.this,
                    "Are you sure? you may need to save current project before opening new project\n To create new project click Yes, To save current project click No", "New project",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION) {
                return;
            }
        }
        this.mainPackage = new ProjectPackage();
        this.mainPackage.dataSet = new TreeMap<Integer, DataClass>();
        this.mainPackage.cases = new LinkedList<Case>();
        this.dataSetFilePath = "";
        this.dataSetFilePathLabel.setText(this.dataSetFilePathLabel.getText().split(":")[0] + ": not saved yet");
        reloadDataClassesList();
        reloadCasesList();
    }//GEN-LAST:event_newProjectMenuItemActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        if (!this.mainPackage.dataSet.isEmpty()) {
            int result = JOptionPane.showConfirmDialog(DataEditor.this,
                    "Are you sure? you may need to save current project before quitting\n To Save and quit click Yes, To quit without saving click No", "Exit",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.YES_OPTION) {
                saveMenuItemActionPerformed(evt);
                System.exit(0);
            } else if (result == JOptionPane.NO_OPTION) {
                System.exit(0);
            }
        }
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void casesListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_casesListValueChanged
        if (!evt.getValueIsAdjusting()) {
            reloadCasesAnswersTable();
        }
    }//GEN-LAST:event_casesListValueChanged

    private void addCaseMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCaseMenuItemActionPerformed
        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose case file");
        fc.setCurrentDirectory(new java.io.File("."));
        fc.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                } else if (f.getName().endsWith(".txt")) {
                    return true;
                } else {
                    return false;
                }
            }

            public String getDescription() {
                return "( *.txt ) case file";
            }
        });

        int returnVal = fc.showOpenDialog(DataEditor.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            if (!file.getName().endsWith(".txt")) {
                infoBox("Only .txt files are accepted.\n", "Warning!");
                return;
            }

            int lineId = -1;
            try {
                List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()));
                String caseName = file.getName();
                Map<Integer, Sample> answers = new TreeMap<>();

                for (String line : lines) {
                    String temp = line.split("\\|")[0];
                    lineId = Integer.parseInt(temp.split("\\ ")[1]);
                    if (!this.mainPackage.dataSet.containsKey(lineId) || answers.containsKey(lineId)) {
                        infoBox("Line :" + lineId + " was not added.\nthe case may contain more than one answer with this Id or there is no category with the specified Id.", "Warning!");
                    } else {
                        answers.put(lineId, new Sample(lineId, line.split("\\|")[1]));
                    }
                }
                int caseId;
                String caseID = JOptionPane.showInputDialog(null, "Enter this Case ID : ", "", 1);
                if (caseID != null) {
                    try {
                        caseId = Integer.parseInt(caseID);
                    } catch (NumberFormatException e) {
                        infoBox("ID Number is not a valid number", "Warning");
                        return;
                    }
                } else {
                    return;
                }
                this.mainPackage.cases.add(new Case(caseId, caseName, answers));
            } catch (FileNotFoundException e) {
                infoBox("Could not find the given file", "Warning!");
                return;
            } catch (IOException | NumberFormatException e) {
                infoBox("Some error happened while reading the file \nLine :" + lineId, "Warning!");
                return;
            } catch (IllegalArgumentException e) {
                infoBox("Confliction : could not accept given file because current project already has categories with given Id's.", "Warning!");
                return;
            } catch (Exception e) {
                infoBox("Unknown error: please check your file on line " + String.valueOf(lineId), "Warning!");
                return;
            }
            reloadDataClassesList();
            reloadCasesList();
        }
    }//GEN-LAST:event_addCaseMenuItemActionPerformed

    private void addNewCaseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addNewCaseButtonActionPerformed
        String input = promptForString("", "New case", "Add new case name");
        if (!input.isEmpty()) {
            int maxKey = -1;
            for (Case casee : this.mainPackage.cases) {
                if (maxKey < casee.id) {
                    maxKey = casee.id;
                }
            }
            maxKey++;
            this.mainPackage.cases.add(new Case(maxKey, input, new TreeMap<Integer, Sample>()));
            reloadCasesList();
        }
    }//GEN-LAST:event_addNewCaseButtonActionPerformed

    private void deleteSelectedCaseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteSelectedCaseButtonActionPerformed
        if (this.casesList.getSelectedValue() != null) {
            Case casee = this.casesList.getSelectedValue();
            if (JOptionPane.showConfirmDialog(DataEditor.this,
                    "Are you sure ? do you want to delete case :" + casee.name, "Delete",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                this.mainPackage.cases.remove(casee);
                reloadCasesList();
            }
        }
    }//GEN-LAST:event_deleteSelectedCaseButtonActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        if (this.dataSetFilePath.isEmpty()) {
            saveAsProjectMenuItemActionPerformed(evt);
        } else {
            try {
                FileOutputStream fileOut = new FileOutputStream(this.dataSetFilePath);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(this.mainPackage);
                out.close();
                fileOut.close();
            } catch (IOException e) {
                infoBox("Error while trying to save project\n" + e.getMessage(), "Error!");
                saveAsProjectMenuItemActionPerformed(evt);
            }
        }
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void exportDatasetMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportDatasetMenuItemActionPerformed

        if (this.mainPackage.dataSet.values().isEmpty()) {
            infoBox("Your data set is empty!", "Warning");
            return;
        }

        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose destination file");
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setCurrentDirectory(new java.io.File("."));
        fc.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                return true;
            }

            public String getDescription() {
                return "( *.txt ) text file.";
            }
        });

        int returnVal = fc.showSaveDialog(DataEditor.this);
        if (returnVal != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File outFile = fc.getSelectedFile();
        try {
            PrintWriter writer = new PrintWriter(outFile.getAbsolutePath() + ".txt", "UTF-8");
            int sampleNumber = 0;

            sampleNumber = this.mainPackage.dataSet.values().stream().map((dataClass) -> dataClass.samples.size()).reduce(sampleNumber, Integer::sum);
            writer.println(this.mainPackage.dataSet.values().size() + "|" + sampleNumber);
            this.mainPackage.dataSet.forEach((k, v) -> {
                writer.println("* " + k + "|" + v.label);
                v.samples.forEach((key, value) -> {
                    writer.println(value.content);
                });
            });
            writer.close();
        } catch (IOException e) {
            infoBox("Error while trying to export dataset.\n" + e.getMessage(), "Error!");
        }
    }//GEN-LAST:event_exportDatasetMenuItemActionPerformed

    private void exportCasesForCasemobileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportCasesForCasemobileMenuItemActionPerformed
        if (this.mainPackage.cases.isEmpty()) {
            infoBox("Your data set is empty!", "Warning");
            return;
        }

        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose destination file");
        fc.setCurrentDirectory(new java.io.File("."));
        fc.setAcceptAllFileFilterUsed(false);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                return true;
            }

            public String getDescription() {
                return "Choose folder";
            }
        });

        int returnVal = fc.showSaveDialog(DataEditor.this);
        if (returnVal != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File outFile = fc.getSelectedFile();
        this.mainPackage.cases.forEach((casee) -> {
            try {
                PrintWriter writer = new PrintWriter(outFile.getAbsolutePath() + "/" + casee.name + ".json", "UTF-8");

                final JSONObject json = new JSONObject();

                mainPackage.dataSet.keySet().forEach((k) -> {
                    try {
                        String val =  mainPackage.dataSet.get(k).label +"|"+ (casee.answers.containsKey(k) ? casee.answers.get(k).content : ("-id-" + k.toString() + "-"));
                        json.put(k.toString(),val );
                        
                    } catch (JSONException e) {
                        infoBox("Error while trying to export case.\n" + e.getMessage(), "Error!");
                    }
                });

                writer.print(json.toString());

                writer.close();
            } catch (IOException e) {
                infoBox("Error while trying to export case.\n" + e.getMessage(), "Error!");
            }
        });
    }//GEN-LAST:event_exportCasesForCasemobileMenuItemActionPerformed

    private void exportCasesForEngineMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportCasesForEngineMenuItemActionPerformed
        if (this.mainPackage.cases.isEmpty()) {
            infoBox("Your data set is empty!", "Warning");
            return;
        }

        final JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose destination file");
        fc.setCurrentDirectory(new java.io.File("."));
        fc.setAcceptAllFileFilterUsed(false);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                return true;
            }

            public String getDescription() {
                return "Choose folder";
            }
        });

        int returnVal = fc.showSaveDialog(DataEditor.this);
        if (returnVal != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File outFile = fc.getSelectedFile();
        this.mainPackage.cases.forEach((casee) -> {
            try {
                PrintWriter writer = new PrintWriter(outFile.getAbsolutePath() + "/" + casee.id + "-" + casee.name + ".txt", "UTF-8");
                String out = "";
                for (Integer key : mainPackage.dataSet.keySet()) {
                    out += "* " + key.toString() + "|" + (casee.answers.containsKey(key) ? casee.answers.get(key).content : ("-id-" + key.toString() + "-")) + "\n";
                }

                writer.print(out);

                writer.close();
            } catch (IOException e) {
                infoBox("Error while trying to export case.\n" + e.getMessage(), "Error!");
            }
        });
    }//GEN-LAST:event_exportCasesForEngineMenuItemActionPerformed

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DataEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DataEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DataEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DataEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DataEditor().setVisible(true);
            }
        });
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel DataClasses;
    private javax.swing.JMenuBar DataEditorMenuBar;
    private javax.swing.JPanel EditorPanel;
    private javax.swing.JMenuItem addCaseMenuItem;
    private javax.swing.JButton addDataClassButton;
    private javax.swing.JButton addDataSampleButton;
    private javax.swing.JButton addNewCaseButton;
    private javax.swing.JTable casesAnswersTable;
    private javax.swing.JList<Case> casesList;
    private javax.swing.JPanel casesPanel;
    private javax.swing.JList<DataClass> dataClassesList;
    private javax.swing.JList<Sample> dataSamplesList;
    private javax.swing.JLabel dataSetFilePathLabel;
    private javax.swing.JButton deleteSelectedCaseButton;
    private javax.swing.JButton deleteSelectedDataClassButton;
    private javax.swing.JButton deleteSelectedSampleButton;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenuItem exportCasesForCasemobileMenuItem;
    private javax.swing.JMenuItem exportCasesForEngineMenuItem;
    private javax.swing.JMenuItem exportDatasetMenuItem;
    private javax.swing.JMenu exportMenu;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JMenuItem newProjectMenuItem;
    private javax.swing.JMenuItem openDatasetMenuItem;
    private javax.swing.JMenuItem openProjectMenuItem;
    private javax.swing.JMenuItem saveAsProjectMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    // End of variables declaration//GEN-END:variables

}
