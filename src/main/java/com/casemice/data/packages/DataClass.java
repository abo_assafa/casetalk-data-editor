/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casemice.data.packages;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author alisafaya
 */

public class DataClass implements Serializable {
    
    public int classId;
    public String label;
    public Map<Integer,Sample> samples;
    
    public DataClass(int id, String label, Map<Integer,Sample> samples){
        this.classId = id;
        this.label = label;
        this.samples = samples;
    }
    
    public static DataClass findDataClass(List<DataClass> list, int Id){
        if (list == null )
            return null;
        DataClass ret = null;
        for (DataClass item: list) {
            if (Id == item.classId) {
                ret = item;
            }
        }
        return ret;
    }
    
    @Override
    public String toString() {
        return this.classId + " : "  + this.label;
    }
}
